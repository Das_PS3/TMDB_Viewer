﻿using System.ComponentModel;
using System.Windows.Media;

namespace TMDB_Viewer.Models
{
    public class IO : INotifyPropertyChanged
    {
        #region Fields

        private string input, output, mediaInfo, mediaPictureURL;

        private ImageSource mediaPicture;

        #endregion Fields

        #region Properties

        public string Input
        {
            get { return input; }
            set
            {
                input = value.ToUpper();
                NotifyPropertyChanged("Input");
            }
        }

        public string MediaInfo
        {
            get { return mediaInfo; }
            set
            {
                mediaInfo = value;
                NotifyPropertyChanged("MediaInfo");
            }
        }

        public ImageSource MediaPicture
        {
            get { return mediaPicture; }
            set
            {
                mediaPicture = value;
                NotifyPropertyChanged("MediaPicture");
            }
        }

        public string MediaPictureURL
        {
            get { return mediaPictureURL; }
            set
            {
                mediaPictureURL = value;
                NotifyPropertyChanged("MediaPictureURL");
            }
        }

        public string Output
        {
            get { return output; }
            set
            {
                output = value;
                NotifyPropertyChanged("Output");
                NotifyPropertyChanged("OutputURL");
            }
        }

        public string OutputURL
        {
            get
            {
                return string.IsNullOrWhiteSpace(output) || output.Length != 40 ? string.Empty : string.Format("http://tmdb.np.dl.playstation.net/tmdb/{0}_00_{1}/{0}_00.xml", input, output);
            }
        }

        #endregion Properties

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion INotifyPropertyChanged implementation
    }
}
