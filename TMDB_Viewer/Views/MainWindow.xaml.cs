﻿using Microsoft.Win32;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Xml;
using TMDB_Viewer.Models;

namespace TMDB_Viewer.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly byte[] tmdbKey = { 0xF5, 0xDE, 0x66, 0xD2, 0x68, 0x0E, 0x25, 0x5B, 0x2D, 0xF7, 0x9E, 0x74, 0xF8, 0x90, 0xEB, 0xF3, 0x49, 0x26, 0x2F, 0x61,
            0x8B, 0xCA, 0xE2, 0xA9, 0xAC, 0xCD, 0xEE, 0x51, 0x56, 0xCE, 0x8D, 0xF2, 0xCD, 0xF2, 0xD4, 0x8C, 0x71, 0x17, 0x3C, 0xDC, 0x25, 0x94, 0x46, 0x5B,
            0x87, 0x40, 0x5D, 0x19, 0x7C, 0xF1, 0xAE, 0xD3, 0xB7, 0xE9, 0x67, 0x1E, 0xEB, 0x56, 0xCA, 0x67, 0x53, 0xC2, 0xE6, 0xB0 };

        private IO io;

        public MainWindow()
        {
            io = new IO();

            DataContext = io;

            InitializeComponent();

            string titleID = Clipboard.GetText().Replace("-", "");

            if ((titleID.Length == 9) && (Regex.IsMatch(titleID, @"^[A-Z]{4}[\d]{5}$")))
                io.Input = titleID;
        }

        #region Private Methods

        private void CleanUp()
        {
            io.MediaPicture = null;
            io.MediaPictureURL = string.Empty;
        }

        private void Convert(byte[] seed)
        {
            var hmac = new Extra.Utilities.HMAC();
            hmac.DoInit(tmdbKey);
            hmac.DoUpdate(seed, 0, seed.Length);

            byte[] hash = hmac.DoFinalButGetHash();

            io.Output = BitConverter.ToString(hash).Replace("-", "");
        }

        private async void FetchXML()
        {
            await Task.Run(() =>
            {
                StringBuilder sb = new StringBuilder();

                var xml = new XmlDocument();
                try
                {
                    xml.Load(io.OutputURL);
                }
                catch (WebException e)
                {
                    io.MediaInfo = e.Message;
                    return;
                }
                catch (XmlException e)
                {
                    io.MediaInfo = e.Message;
                    return;
                }
                finally { CleanUp(); }

                if (xml.DocumentElement.HasAttributes)
                    foreach (XmlAttribute item in xml.DocumentElement.Attributes)
                        sb.AppendLine(item.Name + ": " + item.InnerText);

                foreach (XmlNode node in xml.DocumentElement.ChildNodes)
                {
                    sb.AppendLine(node.Name + ": " + node.InnerText);

                    if (node.Name.ToLower().Contains("icon") && string.IsNullOrWhiteSpace(io.MediaPictureURL))
                        io.MediaPictureURL = node.InnerText;

                    foreach (XmlAttribute attrib in node.Attributes)
                        sb.AppendLine(node.Name + " " + attrib.Name + ": " + attrib.InnerText);
                }

                io.MediaInfo = sb.ToString();

                if (!string.IsNullOrWhiteSpace(io.MediaPictureURL))
                    LoadImage();
            });
        }

        private void LoadImage()
        {
            Application.Current.Dispatcher.BeginInvoke((Action)delegate ()
            {
                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(io.MediaPictureURL, UriKind.Absolute);
                bitmap.EndInit();

                io.MediaPicture = bitmap;
            });
        }

        #endregion Private Methods

        #region Events

        private void Image_Clicked(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(io.MediaPictureURL))
                return;

            var sfd = new SaveFileDialog();
            sfd.FileName = io.Input + "_ICON0.png";

            if (sfd.ShowDialog().Value)
            {
                BitmapEncoder be = new PngBitmapEncoder();
                be.Frames.Add(BitmapFrame.Create((BitmapImage)io.MediaPicture));

                using (var fs = File.Create(sfd.FileName, 4096, FileOptions.SequentialScan))
                    be.Save(fs);
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!Regex.IsMatch(io.Input, @"[A-Z]{4}\d{5}"))
                return;

            byte[] seed = Encoding.UTF8.GetBytes(io.Input + "_00");

            Convert(seed);

            FetchXML();
        }

        #endregion Events
    }
}
